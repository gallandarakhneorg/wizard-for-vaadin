package io.overcoded.vaadin.wizard;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TestContext {
    private String firstName;
    private String lastName;
    private String email;
    private List<Integer> numbers = new ArrayList<>();
}
