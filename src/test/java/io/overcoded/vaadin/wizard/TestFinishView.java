package io.overcoded.vaadin.wizard;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import io.overcoded.vaadin.wizard.config.WizardSimpleContentConfigurationProperties;

import java.util.List;
import java.util.Optional;

public class TestFinishView extends AbstractWizardEndpoint<TestContext> {
    private final H2 h2;

    public TestFinishView(WizardSimpleContentConfigurationProperties properties, TestContext context) {
        super(properties, context);
        h2 = getH2Title("");
        h2.getStyle().set("color", properties.getColor());
        setSizeFull();
        add(getLayout());
    }

    @Override
    protected Optional<Component> getExtraBottomContent() {
        VerticalLayout layout = new VerticalLayout();
        layout.setPadding(false);
        layout.setSpacing(false);
        layout.setWidthFull();
        layout.add(new Text("Your score is:"));
        layout.add(h2);
        layout.setAlignSelf(FlexComponent.Alignment.CENTER, h2);
        return Optional.of(layout);
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        List<Integer> numbers = context.getNumbers();
        int sum = context.getNumbers().stream().mapToInt(Integer::intValue).sum();
        h2.setText(String.valueOf(sum));

    }
}
