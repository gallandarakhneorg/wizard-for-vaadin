package io.overcoded.vaadin.wizard;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import io.overcoded.vaadin.wizard.config.WizardButtonConfigurationProperties;
import io.overcoded.vaadin.wizard.config.WizardFooterConfigurationProperties;

import java.util.List;
import java.util.Objects;

public class WizardFooter<T> extends HorizontalLayout {
    public WizardFooter(WizardFooterConfigurationProperties properties, List<WizardStep<T>> steps, WizardPager pager) {
        boolean firstStep = isFirstStep(steps);
        boolean noStep = hasNoActiveStep(steps);
        boolean lastStep = isLastStep(steps);

        Button back = createButton(properties, properties.getBackButton(), event -> pager.previous());
        Button start = createButton(properties, properties.getStartButton(), event -> pager.start());
        Button next = createButton(properties, properties.getNextButton(), event -> pager.next());
        Button finish = createButton(properties, properties.getFinishButton(), event -> pager.finish());

        setWidthFull();

        boolean hasBackButton = true;
        if (properties.isBackwardSteppingEnabled()) {
            if (firstStep) {
                back.setEnabled(false);
                if (properties.getBackButtonOnFirstStep() == WizardFooterConfigurationProperties.ButtonStatus.DISABLED) {
                    add(back);
                } else {
                    hasBackButton = false;
                }
            } else {
                add(back);
            }
        } else {
            hasBackButton = false;
        }

        HorizontalLayout forwardLayout = new HorizontalLayout();
        forwardLayout.setPadding(false);
        forwardLayout.setMargin(false);
        forwardLayout.setSpacing(true);
        if (firstStep && noStep) {
            forwardLayout.add(start);
        } else {
            if (properties.getStartButtonOnSteps() == WizardFooterConfigurationProperties.ButtonStatus.DISABLED) {
                start.setEnabled(false);
                forwardLayout.add(start);
            }
        }

        if (lastStep) {
            if (properties.getNextButtonOnLastStep() == WizardFooterConfigurationProperties.ButtonStatus.DISABLED) {
                next.setEnabled(false);
                forwardLayout.add(next);
            }
            forwardLayout.add(finish);
        } else {
            if (firstStep && noStep) {
                if (properties.getNextButtonBeforeFirstStep() == WizardFooterConfigurationProperties.ButtonStatus.DISABLED) {
                    next.setEnabled(false);
                    forwardLayout.add(next);
                }
            } else {
                forwardLayout.add(next);
            }
            if (properties.getFinishButtonOnRegularSteps() == WizardFooterConfigurationProperties.ButtonStatus.DISABLED) {
                finish.setEnabled(false);
                forwardLayout.add(finish);
            }
        }

        add(forwardLayout);
        setSpacing(true);
        setPadding(properties.isPaddingEnabled());

        if (hasBackButton) {
            setAlignSelf(Alignment.END, forwardLayout);
            setJustifyContentMode(JustifyContentMode.BETWEEN);
        } else {
            setJustifyContentMode(JustifyContentMode.END);
        }
        if (Objects.nonNull(properties.getBackgroundColor()) && !properties.getBackgroundColor().isBlank()) {
            getStyle().set("background-color", properties.getBackgroundColor());
        }
    }

    private boolean isLastStep(List<WizardStep<T>> steps) {
        int numberOfRemaining = (int) steps
                .stream()
                .filter(wizardStep -> wizardStep.getStatus() == StepStatus.INACTIVE)
                .count();
        int numberOfActive = (int) steps
                .stream()
                .filter(wizardStep -> wizardStep.getStatus() == StepStatus.ACTIVE)
                .count();
        boolean isRegularStep = numberOfRemaining > 0 || numberOfActive > 1;
        return !isRegularStep;
    }

    private boolean isFirstStep(List<WizardStep<T>> steps) {
        return steps.stream().map(WizardStep::getStatus).noneMatch(stepStatus -> stepStatus == StepStatus.COMPLETED);
    }

    private boolean hasNoActiveStep(List<WizardStep<T>> steps) {
        return steps.stream().map(WizardStep::getStatus).noneMatch(stepStatus -> stepStatus == StepStatus.ACTIVE);
    }

    private Button createButton(WizardFooterConfigurationProperties footerProperties, WizardButtonConfigurationProperties buttonProperties, ComponentEventListener<ClickEvent<Button>> eventListener) {
        Button button = new Button();
        if (Objects.nonNull(buttonProperties.getText()) && !buttonProperties.getText().isBlank()) {
            button.setText(buttonProperties.getText());
        }
        if (footerProperties.isIconEnabled() && Objects.nonNull(buttonProperties.getIcon())) {
            button.setIcon(buttonProperties.getIcon().create());
        }
        if (Objects.nonNull(buttonProperties.getVariants()) && !buttonProperties.getVariants().isEmpty()) {
            button.addThemeVariants(buttonProperties.getVariants().toArray(new ButtonVariant[0]));
        }
        if (buttonProperties.isIconAfterText()) {
            button.setIconAfterText(true);
        }
        button.addClickListener(eventListener);
        return button;
    }
}
