package io.overcoded.vaadin.wizard;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.server.StreamResource;
import io.overcoded.vaadin.wizard.config.WizardPictureConfigurationProperties;
import io.overcoded.vaadin.wizard.config.WizardSimpleContentConfigurationProperties;
import lombok.RequiredArgsConstructor;

import java.util.Objects;
import java.util.Optional;

@RequiredArgsConstructor
public abstract class AbstractWizardEndpoint<T> extends Div {
    protected final transient WizardSimpleContentConfigurationProperties properties;
    protected final transient T context;

    protected VerticalLayout getLayout() {
        VerticalLayout layout = new VerticalLayout();
        configureLayout(layout);
        layout.add(getTopBox());
        layout.add(getBottomBox());
        return layout;
    }

    protected void configureLayout(VerticalLayout layout) {
        layout.setSizeFull();
        layout.setPadding(true);
        layout.setMargin(true);
        layout.setSpacing(false);
        layout.setAlignItems(FlexComponent.Alignment.CENTER);
    }

    protected VerticalLayout getTopBox() {
        VerticalLayout layout = new VerticalLayout();
        if (Objects.nonNull(properties.getPicture())) {
            layout.add(getTopBoxImage());
        } else if (Objects.nonNull(properties.getIcon())) {
            layout.add(getTopBoxIcon());
        }
        if (Objects.nonNull(properties.getTitle()) && !properties.getTitle().isBlank()) {
            layout.add(getH2Title(properties.getTitle()));
        }
        decorateTopBox(layout);
        return layout;
    }

    protected VerticalLayout getBottomBox() {
        VerticalLayout layout = new VerticalLayout();
        if (Objects.nonNull(properties.getWidth())) {
            layout.setWidth(properties.getWidth());
        }
        if (Objects.nonNull(properties.getMessage())) {
            layout.add(new Html(properties.getMessage()));
        }
        getExtraBottomContent().ifPresent(layout::add);
        decorateBottomBox(layout);
        return layout;
    }

    protected Optional<Component> getExtraBottomContent() {
        return Optional.empty();
    }

    protected H2 getH2Title(String title) {
        H2 h2 = new H2(title);
        h2.getStyle().set("margin", properties.getTitleTopPadding());
        h2.getStyle().set("padding", properties.getTitleTopPadding());
        h2.getStyle().set("color", properties.getContrastColor());
        return h2;
    }

    protected Icon getTopBoxIcon() {
        Icon icon = properties.getIcon().create();
        icon.setSize(properties.getIconSize());
        return icon;
    }

    protected Image getTopBoxImage() {
        WizardPictureConfigurationProperties picture = properties.getPicture();
        StreamResource imageResource = new StreamResource(picture.getName(), () -> getClass().getResourceAsStream(picture.getPath()));

        Image image = new Image(imageResource, Optional.ofNullable(picture.getAlt()).orElse(picture.getName()));
        if (Objects.nonNull(picture.getWidth())) {
            image.setWidth(picture.getWidth());
        }
        if (Objects.nonNull(picture.getHeight())) {
            image.setHeight(picture.getHeight());
        }
        image.getStyle().set("padding", picture.getPadding());
        return image;
    }

    protected void decorateTopBox(VerticalLayout layout) {
        layout.getStyle().set("border-top-left-radius", properties.getBorderRadius());
        layout.getStyle().set("border-top-right-radius", properties.getBorderRadius());
        layout.getStyle().set("background-color", properties.getColor());
        layout.getStyle().set("color", properties.getContrastColor());
        layout.setAlignItems(FlexComponent.Alignment.CENTER);
        layout.setWidth(properties.getWidth());
    }

    protected void decorateBottomBox(VerticalLayout layout) {
        layout.getStyle().set("color", properties.getColor());
        layout.getStyle().set("border", "1px solid " + properties.getColor());
        layout.getStyle().set("border-bottom-left-radius", properties.getBorderRadius());
        layout.getStyle().set("border-bottom-right-radius", properties.getBorderRadius());
    }
}
