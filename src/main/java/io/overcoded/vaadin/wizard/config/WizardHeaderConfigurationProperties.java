package io.overcoded.vaadin.wizard.config;

import com.vaadin.flow.component.orderedlayout.FlexComponent;
import io.overcoded.vaadin.wizard.StepStatus;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class WizardHeaderConfigurationProperties {
    /**
     * Override background color of footer
     */
    private String backgroundColor = "var(--lumo-contrast-5pct)";
    /**
     * Defines the display order of status columns
     */
    private List<StepStatus> columnOrder = List.of(StepStatus.COMPLETED, StepStatus.ACTIVE, StepStatus.INACTIVE);
    /**
     * If you change the order of status columns, you may need change the aligned items
     */
    private Map<StepStatus, FlexComponent.Alignment> columnAlignment = Map.of(
            StepStatus.COMPLETED, FlexComponent.Alignment.START,
            StepStatus.ACTIVE, FlexComponent.Alignment.CENTER,
            StepStatus.INACTIVE, FlexComponent.Alignment.END
    );
    /**
     * Displays clarification titles over tasks
     */
    private boolean displayTitles = true;
    /**
     * If false, it hides the title if there is no task in the corresponding column
     */
    private boolean displayTitlesWhenNoTaskFound = false;
    /**
     * Pattern for completed tasks column.
     * - %1$d : number of completed tasks
     * - %2$s : name of the latest completed task
     */
    private String completedTitlePattern = "<span><strong>Completed: %1$d</strong></span>";
    /**
     * Pattern for inactive tasks column.
     * - %1$d : number of inactive tasks
     * - %2$s : name of the next inactive task
     */
    private String inactiveTitlePattern = "<span><strong>Remaining: %1$d</strong></span>";
    /**
     * Pattern for active tasks column.
     * - %1$d : number of active tasks
     * - %2$s : name of the latest (non-completed) active task
     */
    private String activeTitlePattern = "<span><strong>%2$s</strong></span>";
    /**
     * String to display in case of no latest completed, active or inactive task.
     * This is the fallback value of previously mentioned %2$s.
     */
    private String fallbackTaskName = "";
    /**
     * Color overrides for tasks by step status
     */
    private Map<StepStatus, String> colors = Map.of(
            StepStatus.COMPLETED, "var(--lumo-success-color-50pct)",
            StepStatus.ACTIVE, "var(--lumo-primary-color)",
            StepStatus.INACTIVE, "var(--lumo-contrast-60pct)"
    );
    /**
     * Color overrides for special tasks which are completed but also active (back)
     */
    private String completedAndActiveColor = "var(--lumo-success-color)";
    /**
     * Turn on grouping of tasks (useful if you have lots of tasks)
     */
    private boolean itemGroupingEnabled = true;
    /**
     * How many item should be visible before the group item
     */
    private Integer maxVisibleItems = 5;
}
