package io.overcoded.vaadin.wizard.config;

import com.vaadin.flow.component.icon.IconFactory;
import com.vaadin.flow.component.icon.VaadinIcon;
import lombok.Data;

@Data
public class WizardInformationConfigurationProperties {
    /**
     * Enable or disable feature to show information details
     */
    private boolean enabled = true;
    /**
     * Summary text of the information details.
     */
    private String text = "Information";
    /**
     * Icon of the information details. Null means the button doesn't have icon.
     */
    private IconFactory icon = VaadinIcon.INFO_CIRCLE;
    /**
     * Color of the icon in the summary text
     */
    private String iconColor = "var(--lumo-primary-text-color)";
    /**
     * Background color of the details block
     */
    private String backgroundColor = "var(--lumo-primary-color-10pct)";
    /**
     * Border color of the details block
     */
    private String borderColor = "var(--lumo-primary-color-50pct)";
    /**
     * Border radius of the details block
     */
    private String borderRadius = "var(--lumo-border-radius-m)";
    /**
     * Open the details automatically or not.
     */
    private boolean opened = true;

}
