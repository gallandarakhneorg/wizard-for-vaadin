package io.overcoded.vaadin.wizard.config;

import com.vaadin.flow.component.icon.IconFactory;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WizardSimpleContentConfigurationProperties {
    /**
     * The main picture of the content
     */
    private WizardPictureConfigurationProperties picture;
    /**
     * Icon of the content (will be ignored if picture has been defined)
     */
    private IconFactory icon;
    /**
     * Size of the icon
     */
    private String iconSize;
    /**
     * The main title, which normally appears under the picture / logo
     */
    private String title;
    /**
     * Padding of the title
     */
    private String titleTopPadding;
    /**
     * Background color of the top box and the border and font color of the bottom box
     */
    private String color;
    /**
     * Text color in the top box
     */
    private String contrastColor;
    /**
     * Top border radius of the top box and bottom border radius of the bottom box
     */
    private String borderRadius;
    /**
     * The width of the boxes
     */
    private String width;
    /**
     * HTML message which should be displayed in the bottom box
     */
    private String message;
}
