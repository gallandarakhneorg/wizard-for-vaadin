package io.overcoded.vaadin.wizard.config;

import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import lombok.Data;

@Data
public class WizardContentConfigurationProperties {
    /**
     * Configuration of information details
     */
    private WizardInformationConfigurationProperties information = new WizardInformationConfigurationProperties();
    /**
     * How to align items in content.
     * Please check the wrappedFormLayout() method for more details.
     */
    private FlexComponent.Alignment alignItems = FlexComponent.Alignment.CENTER;
    /**
     * How should the start view look (logo/icon, title, background and text color, size, message, etc.)
     */
    private WizardSimpleContentConfigurationProperties startView = WizardSimpleContentConfigurationProperties.builder()
            .picture(WizardPictureConfigurationProperties.builder()
                    .name("wizard.png")
                    .path("/images/wizard-colored.png")
                    .width("96px")
                    .height("96px")
                    .padding("var(--lumo-space-m) var(--lumo-space-m) var(--lumo-space-xs) var(--lumo-space-m)")
                    .build())
            .titleTopPadding("0px")
            .title("Welcome to the Wizard!")
            .color("var(--lumo-contrast-60pct)")
            .contrastColor("var(--lumo-primary-contrast-color)")
            .borderRadius("var(--lumo-border-radius-m)")
            .width("35%")
            .message("<p>The Wizard will guide you through this process. Please be sure that you have enough time to finish all the tasks at the same time to prevent any inconsistent state caused by half done processes.<br> If you are ready, please click on the <strong>Start</strong> button. </p>")
            .build();
    /**
     * How should the end view look (logo/icon, title, background and text color, size, message, etc.)
     */
    private WizardSimpleContentConfigurationProperties finishView = WizardSimpleContentConfigurationProperties.builder()
            .icon(VaadinIcon.CHECK_CIRCLE_O)
            .iconSize("96px")
            .titleTopPadding("0px")
            .title("Congratulations!")
            .color("var(--lumo-success-color)")
            .contrastColor("var(--lumo-primary-contrast-color)")
            .borderRadius("var(--lumo-border-radius-m)")
            .width("35%")
            .message("<p>You've <strong>successfully</strong> finished all the steps, everything has been saved. You can safely leave this page.</p>")
            .build();
}
