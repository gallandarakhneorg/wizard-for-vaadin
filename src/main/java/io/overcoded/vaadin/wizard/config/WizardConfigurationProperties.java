package io.overcoded.vaadin.wizard.config;

import lombok.Data;

@Data
public class WizardConfigurationProperties {
    /**
     * Start view should be available or not
     */
    private boolean startViewEnabled = true;
    /**
     * Holder of content related configurations
     */
    private WizardContentConfigurationProperties content = new WizardContentConfigurationProperties();
    /**
     * Holder of header related configurations
     */
    private WizardHeaderConfigurationProperties header = new WizardHeaderConfigurationProperties();
    /**
     * Holder of footer related configurations
     */
    private WizardFooterConfigurationProperties footer = new WizardFooterConfigurationProperties();
}
