package io.overcoded.vaadin.wizard.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WizardPictureConfigurationProperties {
    /**
     * Name of the file
     */
    private String name;
    /**
     * Path of the image (on classpath)
     */
    private String path;
    /**
     * Display width of the image
     */
    private String width;
    /**
     * Display height of the image
     */
    private String height;
    /**
     * Alt text of the image if not found
     */
    private String alt;
    /**
     * Padding of the image
     */
    private String padding;
}
