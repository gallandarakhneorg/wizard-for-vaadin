package io.overcoded.vaadin.wizard;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.avatar.AvatarGroup;
import com.vaadin.flow.component.avatar.AvatarGroupVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import io.overcoded.vaadin.wizard.config.WizardHeaderConfigurationProperties;

import java.util.*;
import java.util.stream.Collectors;

public class WizardHeader<T> extends HorizontalLayout {
    private final transient WizardHeaderConfigurationProperties properties;

    public WizardHeader(WizardHeaderConfigurationProperties properties, List<WizardStep<T>> steps) {
        this.properties = properties;
        Map<StepStatus, List<WizardStep<T>>> stepsByStatus = steps.stream().collect(Collectors.groupingBy(WizardStep::getStatus));
        List<WizardStep<T>> completedSteps = stepsByStatus.getOrDefault(StepStatus.COMPLETED, List.of());
        List<WizardStep<T>> inactiveSteps = stepsByStatus.getOrDefault(StepStatus.INACTIVE, List.of());
        List<WizardStep<T>> activeSteps = stepsByStatus.getOrDefault(StepStatus.ACTIVE, List.of());

        Map<StepStatus, VerticalLayout> layouts = Map.of(StepStatus.COMPLETED, createCompletedStepHolder(properties, completedSteps), StepStatus.ACTIVE, createActiveStepHolder(properties, activeSteps), StepStatus.INACTIVE, createInactiveStepHolder(properties, inactiveSteps));

        properties.getColumnOrder().stream().map(layouts::get).forEach(this::add);
        setJustifyContentMode(JustifyContentMode.EVENLY);
        setSpacing(false);
        setPadding(false);
        setWidthFull();
        if (Objects.nonNull(properties.getBackgroundColor()) && !properties.getBackgroundColor().isBlank()) {
            getStyle().set("background-color", properties.getBackgroundColor());
        }
    }

    private VerticalLayout createCompletedStepHolder(WizardHeaderConfigurationProperties properties, List<WizardStep<T>> completedSteps) {
        Html title = createTitle(completedSteps, properties.getCompletedTitlePattern(), properties.getFallbackTaskName());
        VerticalLayout stepHolder = getStepHolder(title, completedSteps);
        stepHolder.setAlignItems(properties.getColumnAlignment().get(StepStatus.COMPLETED));
        return stepHolder;
    }

    private VerticalLayout createActiveStepHolder(WizardHeaderConfigurationProperties properties, List<WizardStep<T>> activeSteps) {
        Html title = createTitle(activeSteps, properties.getActiveTitlePattern(), properties.getFallbackTaskName());
        VerticalLayout stepHolder = getStepHolder(title, activeSteps);
        stepHolder.setAlignItems(properties.getColumnAlignment().get(StepStatus.ACTIVE));
        return stepHolder;
    }

    private VerticalLayout createInactiveStepHolder(WizardHeaderConfigurationProperties properties, List<WizardStep<T>> inactiveSteps) {
        ArrayList<WizardStep<T>> reverseSteps = new ArrayList<>(inactiveSteps);
        Collections.reverse(reverseSteps);
        Html title = createTitle(reverseSteps, properties.getInactiveTitlePattern(), properties.getFallbackTaskName());
        VerticalLayout stepHolder = getStepHolder(title, inactiveSteps);
        stepHolder.setAlignItems(properties.getColumnAlignment().get(StepStatus.INACTIVE));
        return stepHolder;
    }

    private Html createTitle(List<WizardStep<T>> completedSteps, String titlePattern, String fallbackTaskName) {
        String nameValue = completedSteps.stream().reduce((first, second) -> second).map(WizardStep::getName).orElse(fallbackTaskName);
        String titleContent = titlePattern.formatted(completedSteps.size(), nameValue);
        return new Html(titleContent);
    }

    private VerticalLayout getStepHolder(Html title, List<WizardStep<T>> steps) {
        VerticalLayout stepHolder = new VerticalLayout();
        stepHolder.setAlignItems(Alignment.START);
        stepHolder.setSpacing(true);
        stepHolder.setWidth("33%");

        AvatarGroup avatarGroup = new AvatarGroup();
        avatarGroup.addThemeVariants(AvatarGroupVariant.LUMO_LARGE);
        avatarGroup.setWidth("auto");

        // grouping to many items if enabled
        if (properties.isItemGroupingEnabled()) {
            avatarGroup.setMaxItemsVisible(properties.getMaxVisibleItems());
        }
        steps.forEach(wizardStep -> avatarGroup.add(wizardStep.createAvatarGroupItem()));

        // display title only if enabled
        if (properties.isDisplayTitles()) {
            if (steps.isEmpty()) {
                if (properties.isDisplayTitlesWhenNoTaskFound()) {
                    stepHolder.add(title);
                }
            } else {
                stepHolder.add(title);
            }
        }
        stepHolder.add(avatarGroup);
        return stepHolder;
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        super.onAttach(attachEvent);
        properties.getColors().forEach(this::overrideColor);
        overrideColor(4, properties.getCompletedAndActiveColor());
    }

    private void overrideColor(StepStatus key, String value) {
        overrideColor(key.getColorIndex(), value);
    }

    private void overrideColor(int colorIndex, String value) {
        UI.getCurrent().getElement().getStyle().set(getUserColorProperty(colorIndex), value);
    }

    private String getUserColorProperty(int colorIndex) {
        return "--vaadin-user-color-" + colorIndex;
    }
}
