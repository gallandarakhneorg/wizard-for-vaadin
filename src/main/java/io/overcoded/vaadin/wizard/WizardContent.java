package io.overcoded.vaadin.wizard;

import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import java.util.Objects;

public class WizardContent<T> extends VerticalLayout {
    public WizardContent(WizardStep<T> step) {
        setSizeFull();
        setJustifyContentMode(JustifyContentMode.START);
        if (Objects.nonNull(step)) {
            Scroller scroller = new Scroller(step.getLayout());
            scroller.setWidthFull();
            scroller.setScrollDirection(Scroller.ScrollDirection.VERTICAL);
            add(scroller);
        } else {
            add(new H3("Something went wrong!"));
        }
    }
}
